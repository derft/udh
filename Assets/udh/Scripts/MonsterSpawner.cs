﻿// Used to spawn monsters repeatedly. The 'monsterGoals' will be passed to the
// monster after spawning it, so that the monster knows where to move (which
// lane to walk along).

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Networking;
using System.Linq;

public class MonsterSpawner : NetworkBehaviour
{
    // the monster to spawn
    public Monster monster;
    public float interval = 5;
    public Transform monsterGoal; // passed to monsters
    public string NavMeshAreaPreferred = ""; // MidLane, etc.

    private int WaveNumber = 1;

    private IEnumerator coroutine;

    [SerializeField] List<Monster> MonstersInWave;


    public override void OnStartServer()
    {
        
//        InvokeRepeating("Spawn", interval, interval);
        coroutine = Spawn(1);
        StartCoroutine(coroutine);
    }


//    [Server]
//    void Spawn() {
//        var go = (GameObject)Instantiate(monster.gameObject, transform.position, Quaternion.identity);
//        go.name = monster.name; // remove "(Clone)" suffix
//        go.GetComponent<Monster>().goal = monsterGoal;
//
//        // temporary workaround for bug #950004
//        go.GetComponent<NavMeshAgent>().Warp(transform.position);
//
//        // set preferred navmesh area costs to 1
//        int index = NavMesh.GetAreaFromName(NavMeshAreaPreferred);
//        if (index != -1)
//            go.GetComponent<NavMeshAgent>().SetAreaCost(index, 1);
//
//        NetworkServer.Spawn(go);
//    }


    [Server]
    IEnumerator Spawn(int waitTime)
    {
        
        yield return new WaitForSeconds(30.0f);
        
        
        while (true)
        {
            for (int i = 0; i < WaveNumber % 4; i++)
            {
                var go = (GameObject) Instantiate(MonstersInWave[WaveNumber - 1].gameObject, transform.position,
                    Quaternion.identity);
                go.name = monster.name; // remove "(Clone)" suffix
                go.GetComponent<Monster>().goal = monsterGoal;

                // temporary workaround for bug #950004
                go.GetComponent<NavMeshAgent>().Warp(transform.position);

                // set preferred navmesh area costs to 1
                var index = UnityEngine.AI.NavMesh.GetAreaFromName(NavMeshAreaPreferred);
                if (index != -1)
                    go.GetComponent<UnityEngine.AI.NavMeshAgent>().SetAreaCost(index, 1);

                NetworkServer.Spawn(go);
                yield return new WaitForSeconds(waitTime);
            }

            WaveNumber++;

            yield return new WaitForSeconds(interval);
        }
    }
}